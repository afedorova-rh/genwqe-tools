#! /bin/sh
# genwqe-tools basics

PACKAGES="genwqe-tools"
# SERVICES=""

# source the test script helpers
# requires beakerlib package
. /usr/bin/rhts-environment.sh || exit 1
. /usr/share/beakerlib/beakerlib.sh || exit 1

rlJournalStart
	rlPhaseStartSetup
		for p in $PACKAGES ; do
			rlAssertRpm $p
		done
		rlRun "TmpDir=\$(mktemp -d)" 0 "Creating tmp directory"
		rlRun "pushd $TmpDir"
	rlPhaseEnd
	rlPhaseStartTest "Smoke, sanity and function tests"
                rlRun "genwqe_cksum --version" 0 "version check"
                rlRun "genwqe_echo --version" 0 "version check"
                rlRun "genwqe_ffdc --version" 0 "version check"
                rlRun "genwqe_gunzip --version" 0 "version check"
                rlRun "genwqe_memcopy --version" 0 "version check"
                rlRun "genwqe_peek --version" 0 "version check"
                rlRun "genwqe_poke --version" 0 "version check"
                rlRun "genwqe_update --version" 0 "version check"
                rlRun "zlib_mt_perf --version" 0 "version check"

		# check man page
		for m in genwqe_cksum genwqe_echo genwqe_ffdc genwqe_gunzip genwqe_gzip genwqe_memcopy genwqe_mt_perf genwqe_peek genwqe_poke genwqe_test_gz genwqe_update gzFile_test zlib_mt_perf ; do
			rlRun "man -P head $m" 0 "Show the $m man page"
		done

		# check for sane license and readme file
		rlRun "head /usr/share/licenses/genwqe-tools/LICENSE" 0 "Check for license file"
	rlPhaseEnd
	rlPhaseStartCleanup
		rlRun "popd"
		rlRun "rm -fr $TmpDir" 0 "Removing tmp directory"
	rlPhaseEnd
rlJournalPrintText
rlJournalEnd
